
## Getting the templates

```
## Grab the templates
git clone https://ari-harmonic@bitbucket.org/ari-harmonic/hackathon-1-templates.git

## Removes the reference to the template files
git remote remove origin
```

Relabel the folder with a more appropriate name


## Structure

```
    00_data_prep.ipynb
    01_dqa_eda.ipynb
    02_report.ipynb
    README.md
    requirements.txt
    data/
        processed/
        raw/
    docs/
```



## Write your own readme here

Here are some of the areas that I cover usually:

    * Description
    * How to Run It
    * How to Run the Unit Tests (if any)
    * Key Dependencies
    * Future Enhancements